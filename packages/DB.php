<?php
namespace packages;
use config\Config;

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 13.04.2017
 * Time: 10:59
 */
class DB
{
    protected $connection;
    protected $is_connected = false;

    protected static $DB;

    protected function __construct()
    {

    }

    public static function getInstance()
    {
        if (!is_null(self::$DB)) return self::$DB;
        self::$DB = new self();
        return self::$DB;
    }

    public function is_connected()
    {
        return $this->is_connected;
    }
    public function getConnection()
    {
        if ($this->connection) return $this->connection;
        $this->connection = mysqli_connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASS, Config::DB_NAME);
        $this->getConnection()->set_charset('UTF8');
        if (!$this->connection) {
            echo "Ошибка: Невозможно установить соединение с MySQL." . PHP_EOL;
            echo "Код ошибки errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Текст ошибки error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        $this->is_connected = true;
        return $this->connection;
    }


}