<?php
require __DIR__ . '/../vendor/autoload.php';


$conn = \packages\DB::getInstance()->getConnection();

$Users = \Models\User::getAll();
$Date = date_create_from_format('Y-m-d', '2017-01-10');
$Events = \Models\Event::getAll($Date);

$Users = array_map(function($User) {
    $User['working'] = 0;
    $User['break'] = 0;
    $User['current'] = null;
    $User['current_type'] = null;
    return $User;
}, $Users);

function getDateObj($string)
{
    return date_create_from_format('Y-m-d H:i:s', $string);
}


foreach ($Events as $event) {
    $user_id = $event['user_id'];
    if (!$Users[$user_id]['current']) {
        $Users[$user_id]['current'] = getDateObj($event['event_at']);
        $Users[$user_id]['current_type'] = $event['type'];
    }
    /**
     * @var \DateTime $Current
     */
    $Current = $Users[$user_id]['current'];
    $EventDate = getDateObj($event['event_at']);
    $diff  = $EventDate->getTimestamp() - $Current->getTimestamp();
    switch ($Users[$user_id]['current_type']) {
        case 'working':
            $Users[$user_id]['working'] += $diff;
            break;
        case 'break';
            $Users[$user_id]['break'] += $diff;
    }

    $Users[$user_id]['current'] = $EventDate;
    $Users[$user_id]['current_type'] = $event['type'];
}
?>
<table>
    <tr>
        <td>Сотрудник</td>
        <td>Работал</td>
        <td>Отдыхал</td>
    </tr>
    <?php foreach ($Users as $user) { ?>
        <tr>
            <td><?=$user['name']?></td>
            <td><?=$user['working']/3600?></td>
            <td><?=$user['break']/3600?></td>
        </tr>
    <?php } ?>
</table>
