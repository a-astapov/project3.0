<?php

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 13.04.2017
 * Time: 11:32
 */
namespace Models;
class User
{
    public static function getAll()
    {
        $result = \packages\DB::getInstance()->getConnection()->query('SELECT * FROM user');
        $users = [];
        while ($row = $result->fetch_assoc()) {
            $users[$row['id']] = $row;
        }
        return $users;
    }
}