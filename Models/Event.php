<?php

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 13.04.2017
 * Time: 11:33
 */
namespace Models;
class Event
{
    public static function getAll(\DateTime $Date)
    {
        $start = $Date->format('Y-m-d').' 00:00:00';
        $end = $Date->format('Y-m-d').' 23:59:59';

        $result = \packages\DB::getInstance()->getConnection()->query("SELECT * FROM event where event_at between '$start' and '$end' ORDER BY user_id, event_at");
        if (!$result) return [];
        $events = [];
        while ($row = $result->fetch_assoc()) {
            $events[$row['id']] = $row;
        }
        return $events;
    }
}